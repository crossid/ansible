## CrossID Deployment

Automated deployment tools to setup CrossID via [Ansible](https://github.com/ansible/ansible).

## PlayBooks

### Non Dockerized Deployment

![Alt text](docs/standalone.png "Standalone")

Pre Requisites:

- Ansible > 2.5
- CentOS / RHEL 7+


This is a non dockerized deployment used mainly for on prem setup where docker is unavailable,
It works offline so internet connectivity is unnecessary.

1. Offline installation requires the following files to be stored in `pkgs` folder:

```bash
crossid_linux-amd64.tar.gz
crossid-webui.tar.gz
traefik_linux-amd64
```

1. Edit the servers inventory `hosts` file, all components (_backend_, _webui_ and _proxy_) can be installed on the same server.

1. Edit `group_vars/all.yml` properties.

1. Deploy the playbook by:

        ansible-playbook -i hosts standalone.yml -t backend -t webui -t proxy

**Note**: If you want to use your own LBS / proxy you can skip the proxy setup by removing the `-t proxy` tag.


During playbook execution, a secret token is generated by the `generate token` task, copy this token.

Once setup is completed, expect an output such:

```bash
      PLAY RECAP ************************************************************************************************************************************************
      serverName                     : ok=41   changed=31   unreachable=0    failed=0   
```

Access the admin webui interface by `http://yourhost/admin` and paste the generated copied token into the input field.