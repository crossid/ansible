# CrossID Backend

This role deploys a non dockerized CrossID backend.


## Requirements

- Ansible > 2.5
- CentOS / RHEL 7+


## Role Variables

Please refer to `defaults/main.yml`

### Single CrossID node

Deploying a single CrossID node is trivial; just add the role to your playbook and go.


## TODO

- Upgrade mode: should extract the binary, stop service, create new sym link, restart service.
- Get rid of tenant specific job scheduler once backend supports it
- syslog support
- support multi node deployment
- "restart crossid" handler should probably get removed