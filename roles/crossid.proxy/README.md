# Proxy Role

Setup a load balancer that proxies requests to backends and frontends.


# TODO

- Supports more than one node.
- Proxy rewrites dns when proxing to its backends, causing crossid backend to loose tenant host name.
- Check upgrades